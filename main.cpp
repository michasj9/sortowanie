#include <iostream>

using namespace std;

double t[10];

void sortowanie_babelkowe_r();
void sortowanie_babelkowe_m();


int main()
{
    cout << "Podaj 10 liczb:" << endl;

    for(int i=0; i<10; i++) cin >> t[i];

    sortowanie_babelkowe_r();
    sortowanie_babelkowe_m();

    return 0;
}

void sortowanie_babelkowe_r()
{
    double bufor;
    for(int i=0; i<9; i++)
    {
        for(int j=9; j>=1; j--)
        {
            if(t[j]<t[j-1])
            {
                bufor = t[j-1];
                t[j-1] = t[j];
                t[j] = bufor;
            }
        }
    }

    cout << "Sortowanie rosnaco: ";
    for(int i=0; i<10; i++) cout << t[i] << " ";
    cout << endl;
}

void sortowanie_babelkowe_m()
{
    double bufor;
    for(int i=0; i<9; i++)
    {
        for(int j=9; j>=1; j--)
        {
            if(t[j]>t[j-1])
            {
                bufor = t[j-1];
                t[j-1] = t[j];
                t[j] = bufor;
            }
        }
    }

    cout << "Sortowanie malejaco: ";
    for(int i=0; i<10; i++) cout << t[i] << " ";
    cout << endl;
}
